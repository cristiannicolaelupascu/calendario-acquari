const logger = require('./logger')
const fs = require('fs')
const {google} = require('googleapis')
const readline = require('readline')
const events = require('./events')

module.exports = {
    
    getNewToken:function(settings,oAuth2Client, callback){
        const auth_url = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope:        settings.scope
        })
        console.log('Authorize this app: ', auth_url)
        const read_line = readline.createInterface({
            input:  process.stdin,
            output: process.stdout
        })
        read_line.question("Enter the code from the page: ", (code) => {
            read_line.close()
            oAuth2Client.getToken(code, (err, token) => {
                if (err)
                    return logger.logError(settings.logPath,err)
                else {
                    fs.writeFile(settings.tokenPath,JSON.stringify(token), (err) => {
                        if (err)
                            return logger.logError(settings.logPath,err)
                    })
                    callback(oAuth2Client)
                }
    
            })
        })
    },


    activate:function(settings){
        fs.readFile(settings.credentialsPath, (err, content) => {
            if (err) {
                return logger.logError(setttings.logPath,err)
            }
            else{
                logger.logInfo(settings.logPath,"Authorizing application")
                this.authorize(settings,JSON.parse(content),events.addEvents)
            }
        })
    },
    
    
    authorize:function(settings,credentials, callback){
        logger.logInfo(settings.logPath,"Accessing to Google auth system")
        const { client_secret,
                client_id,
                redirect_uris } = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
            client_id,
            client_secret,
            redirect_uris[0]
        )
        fs.readFile(settings.tokenPath, (err, token) => {
            if (err) {
                logger.logInfo(settings.logPath,"Getting new token")
                return this.getNewToken(settings,oAuth2Client, callback)
            }
            else { 
                logger.logInfo(settings.logPath,"Activating callback")
                oAuth2Client.setCredentials(JSON.parse(token))
                callback(settings,oAuth2Client)
            }
        })
    }

}


    

